# Snapchat posisjonslogg

Plotter ut posisjonsloggen din fra data snapchat har samlet om deg

![skjermbilde](skjermbilde.png)

## Bruk

last ned dataen din fra snapchat
(https://accounts.snapchat.com/accounts/downloadmydata)

legg inn `json/location_history.json`fra arkivet inn i `src/logg`

start vha. `npm i && npm start`