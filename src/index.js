import { datahenter } from './datahenter.js';

let mymap = L.map('kart').setView([62, 10], 5);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
  attribution:
    'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  maxZoom: 18,
  id: 'mapbox/streets-v11',
  accessToken: 'pk.eyJ1Ijoiam9ua2Fub24iLCJhIjoiY2s2Z21yd3M0MmwwMTNtb2IwbHRuMm93ZyJ9.DhEcgwcwQAqi83thD5klpg'
}).addTo(mymap);

let sirkel = {
  stil: {
    color: 'red',
    fillColor: 'red',
    fillOpacity: 1
  },
  origo: [0, 0],
  radius: 10
};
for (let punkt of datahenter.posisjonslogg()) {
  sirkel.origo = punkt;
  L.circle(sirkel.origo, sirkel.radius, sirkel.stil).addTo(mymap);
}
