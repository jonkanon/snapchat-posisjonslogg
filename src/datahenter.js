import fs from 'fs';
const STI = 'src/logg/location_history.json';

class Datahenter {
  constructor() {
    this.data = this.hentData();
    this.formatertdata = this.formaterData();
  }
  hentData() {
    return JSON.parse(fs.readFileSync(STI))['Locations You Have Visited'];
  }
  formaterData() {
    let punkter = [];
    let oppdeltPunkt = [];
    // console.log(this.data);
    for (let punkt of this.data) {
      oppdeltPunkt = punkt['Latitude, Longitude'].split(' ');
      punkter.push([oppdeltPunkt[0], oppdeltPunkt[4]]);
    }
    return punkter;
  }
  get posisjonslogg() {
    return this.formaterData;
  }
}
export let datahenter = new Datahenter();
